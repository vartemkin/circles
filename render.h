#ifndef RENDER_H
#define RENDER_H

#include <QGLWidget>
#include <cmath>

#include <GL/glu.h>


#include <QDebug>

class Render : public QGLWidget
{
    Q_OBJECT
public:
    explicit Render(QWidget *parent = 0);
    ~Render();

    void timeout();

    void drawCircle(float r);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private:
    double t;
};

#endif // RENDER_H
