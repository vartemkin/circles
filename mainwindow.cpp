#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_timer = new QTimer();
    connect(m_timer, SIGNAL(timeout()), this, SLOT(timeout()));

    m_timer->start(50);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timeout()
{
    ui->widget->timeout();
}
