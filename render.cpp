#include "render.h"

Render::Render(QWidget *parent) :
    QGLWidget(parent)
{
    t = 0;
}

Render::~Render()
{
    //
}

void Render::timeout()
{
    t += 1;
    if (t > 360) t = 1;

    updateGL();
}

void Render::drawCircle(float r)
{
    glBegin(GL_LINE_LOOP);
    for (float f=0; f<M_PI*2; f+=0.1) {
        glVertex3f(sin(f)*r, cos(f)*r, 0.0);
    }
    glEnd();
}

void Render::initializeGL()
{
    qglClearColor(Qt::black);

    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);
    //glEnable(GL_LIGHTING);
    //glEnable(GL_LIGHT0);
    //glEnable(GL_MULTISAMPLE);
    //static GLfloat lightPosition[4] = { 0.5, 5.0, 7.0, 1.0 };
    //glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
}

void Render::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0.0, 0.0, -10.0);


    int k = 50;
    float r = 4;
    bool type = true;

    for (int i=0; i<k && r > 0; i++)
    {
        if (type) {
            glRotatef(t, 0.0, 1.0, 0.0);
            glColor3f(1.0, 0.0, 0.0);
        }
        else
        {
            glRotatef(t, 1.0, 0.0, 0.0);
            glColor3f(0.0, 1.0, 0.0);
        }

        drawCircle(r);

        r -= 0.2;
        type = !type;
    }

    //qDebug() << t;


}

void Render::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(50.0, (double)width / (double)height, 1.0, 500.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void Render::mousePressEvent(QMouseEvent *event)
{
    //
}

void Render::mouseMoveEvent(QMouseEvent *event)
{
    //
}
