#-------------------------------------------------
#
# Project created by QtCreator 2014-02-24T14:01:25
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = circles
TEMPLATE = app

LIBS += -lGL -lGLU

SOURCES += main.cpp\
        mainwindow.cpp \
    render.cpp

HEADERS  += mainwindow.h \
    render.h

FORMS    += mainwindow.ui
